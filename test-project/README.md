# Vue Dev Project
Developer sandbox boilerplate integrated with [Jest](https://jestjs.io/) and [Less](http://lesscss.org/)
***

### Installation:
```git clone git@bitbucket.org:styrbjornnordin/vue-test.git```

### Bash:
- install dependencies: ```npm install```
- start application / serve with hot reload at localhost:8080: ```npm start```
- build for production with minification: ```npm run build```
- build for production and view the bundle analyzer report: ```npm run build --report```

### Testing:

Test folder: ___vue-test\test-project\test\unit\specs___
Example file: ```TitleOfComponent.spec.js```

- run all tests: ```npm test```
- run unit tests: ```npm run unit```

https://vue-test-utils.vuejs.org/guides/testing-single-file-components-with-jest.html

### VSCode extensions:
- [Vetur](https://vuejs.github.io/vetur)
- [Less IntelliSense](github.com/mrmlnc/vscode-less)
- [TSLint](https://github.com/palantir/tslint)
- [ESLint](https://github.com/palantir/tslint)
- [Jest](https://github.com/facebook/jest)

### Vue dox:
For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
https://vuejs.org/
