import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/pages/MainPage'
import RandomPage from '@/components/pages/RandomPage'
import OtherPage from '@/components/pages/OtherPage'
import TestPage from '@/components/pages/TestPage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage
    },
    {
      path: '/other-page',
      name: 'OtherPage',
      component: OtherPage
    },
    {
      path: '/random-page',
      name: 'RandomPage',
      component: RandomPage
    },
    {
      path: '/test-page',
      name: 'TestPage',
      component: TestPage
    }
  ]
})
