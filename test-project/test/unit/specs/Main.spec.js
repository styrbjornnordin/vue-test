import Vue from 'vue'
import Router from 'vue-router'
import { shallowMount, RouterLinkStub } from '@vue/test-utils'

import Main from '@/components/Main'

Vue.use(Router)

describe('Main', () => {
  test('is a Vue instance', () => {
    const wrapper = shallowMount(Main, {
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
