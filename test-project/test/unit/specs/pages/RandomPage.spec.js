import { mount } from '@vue/test-utils'

import RandomPage from '@/components/pages/RandomPage'

describe('The last element', () => {
  const wrapper = mount(RandomPage)

  test('should render a specific static message', () => {
    expect(wrapper.find('[data-test="random-page-test"]')
      .text())
      .toEqual('This is a message for testing')
  })
})
