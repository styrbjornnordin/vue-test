import { mount } from '@vue/test-utils'

import MainPage from '@/components/pages/MainPage'

describe('The main page', () => {
  const wrapper = mount(MainPage)
  const img = wrapper.find('img')

  it('should contain an image that is a png', () => {
    expect(img.attributes('src')).toContain('.png')
  })
})
